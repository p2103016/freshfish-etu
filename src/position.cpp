#include "position.hpp"

#include <stdexcept>

Position voisine(const Position& pos, int index) {
  Position res = pos ;
  if (index == 0) {
    return Position(pos.first - 1, pos.second) ;
  }
  else if (index == 1) {
    return Position(pos.first, pos.second + 1) ;
  }
  else if (index == 2) {
    return Position(pos.first + 1, pos.second) ;
  }
  else if (index == 3) {
    return Position(pos.first, pos.second - 1) ;
  }
  else {
    throw std::invalid_argument("index must be in [0,3]") ;
  }

  return res ;
}

std::ostream& operator<<(std::ostream& out, const Position& pos) {
  out << "{" << pos.first << "," << pos.second << "}" ;
  return out ;
}

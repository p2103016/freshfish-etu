#ifndef FRESHFISH_PARTIE_HPP
#define FRESHFISH_PARTIE_HPP

#include "plateau.hpp"
#include "pioche.hpp"
#include "joueur.hpp"

#include <vector>

struct Joueur ;

struct Partie {
  Partie(int nb_joueurs) ;

  //tourne en boucle jusqu'à la fin de la partie
  void lancer() ;
  int gagnant() const ; // Retourne l'index du joueur gagnant (avec le moins de points)
  int calculer_score(int index) const ; // Retourne le score du joueur d'index donné

  //données de la partie
  Plateau plateau ;
  Pioche pioche ;
  std::vector<Joueur> joueurs;
  int joueur_courant ;
} ;

#endif

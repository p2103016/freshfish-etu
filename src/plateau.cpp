#include "constantes.hpp"
#include "plateau.hpp"
#include "console_pad.hpp"

#include <stdexcept>
#include <sstream>
#include <queue>
#include <stack>
#include <ios>
#include <iomanip>

struct Tuile_marquee
{
  /*
  Structure pour sauvegarder la profondeur et la remontée de chaque case
  */
  int profondeur;
  int remontee;
  Tuile_marquee(int profondeur, int remontee) : profondeur(profondeur), remontee(remontee) {}
  Tuile_marquee() : profondeur(-1), remontee(-1) {}
};

static bool est_pas_amenagee(const Tuile &t)
{
  /*
  @brief Vérifie si une tuile est vide ou route ou réservée
  @param t La tuile à vérifier
  @return true si la tuile est vide ou route ou réservée, false sinon
  */
  return t.amenagement == Amenagement::VIDE || t.amenagement == Amenagement::ROUTE || t.amenagement == Amenagement::RESERVEE;
}

void explorer(std::map<Position, Tuile_marquee> &marquees, std::map<Position, Tuile> &tuiles, Position pos)
{
  /*
  @brief Parcours en profondeur du plateau pour marquer la profondeur et la remontée de chaque case
  @param marquees La map des cases marquées (position : profondeur et remontée)
  @param tuiles La map du plateau de jeu
  @param pos La position actuel du parcours
  */
  for (int i = 0; i < 4; ++i)
  {
    Position v = voisine(pos, i);
    if (tuiles.find(v) != tuiles.end())
    { // Si la tuile existe
      if (marquees.find(v) == marquees.end())
      {
        if (est_pas_amenagee(tuiles[v]))
        { // Arc utilisé
          marquees[v] = Tuile_marquee(marquees[pos].profondeur + 1, marquees[pos].profondeur + 1);
          explorer(marquees, tuiles, v);
          marquees[pos].remontee = std::min(marquees[pos].remontee, marquees[v].remontee);
        } // Arc impasse sinon
      }
      else
      { // Arc ignoré
        // Pas besoin de vérifier si elle est vide seul une case vide ou route peut être marquée
        marquees[pos].remontee = std::min(marquees[pos].remontee, marquees[v].profondeur);
      }
    }
  }
}

std::map<Position, Tuile_marquee> parcours_profondeur(std::map<Position, Tuile> &tuiles, const Position &pos_depart)
{
  /*
  @brief Début du parcours en profondeur du plateau pour marquer la profondeur et la remontée de chaque case
  @param tuiles La map du plateau de jeu
  @param pos_depart La position de départ du parcours
  @return La map des cases marquées (position : profondeur et remontée)
  */
  std::map<Position, Tuile_marquee> marquees;
  marquees[pos_depart] = Tuile_marquee(0, 0);
  explorer(marquees, tuiles, pos_depart);
  return marquees;
}

int Plateau::parcours_route(const Amenagement objectif, int parcours, const Position &pos, const Position &prec) const
{
  /*
  @brief Parcours en profondeur du plateau pour trouver le plus court chemin vers un objectif
  @param objectif L'aménagement à atteindre
  @param parcours Le nombre de cases parcourues
  @param pos La position actuel du parcours
  @param prec La position précédente du parcours
  @return Le nombre de cases parcourues pour atteindre l'objectif (max 14)
  */
  if (parcours == 14)
  {
    return 14;
  }
  int parcours_final = 14;
  for (int i = 0; i < 4; i++)
  {
    Position v = voisine(pos, i);
    if (v.first == prec.first && v.second == prec.second)
    {
      continue;
    }
    if (tuiles.find(v) != tuiles.end())
    {
      if (est_pas_amenagee(tuiles.at(v)))
      {
        parcours_final = std::min(parcours_route(objectif, parcours + 1, v, pos), parcours_final);
      }
      else if (tuiles.at(v).amenagement == objectif)
      {
        return parcours;
      }
    }
  }
  return parcours_final;
}

int Plateau::get_point(const Position &pos) const
{
  /*
  @brief Calcul le nombre de points d'une boutique
  @param pos La position de la boutique
  @return Le nombre de points de la boutique
  */
  if (tuiles.find(pos) == tuiles.end())
  {
    throw std::invalid_argument(
        "cette tuile n'existe pas");
  }

  const Tuile &t = tuiles.at(pos);
  Ressource res = ressource(t.amenagement);
  Amenagement objectif = specifie(Amenagement::PRODUCTEUR, res);

  int point = parcours_route(objectif, 0, pos, pos);

  return point;
}

static void afficher_parcour(Plateau &p, std::map<Position, Tuile_marquee> marquees)
{
  /*
  @brief Fonction de debug pour afficher le marquage des case lors du parcours en profondeur du plateau
  @param p Le plateau de jeu
  @param marquees La map des cases marquées (position : profondeur et remontée)
  */
  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < 10; j++)
    {
      Position p = Position(i, j);
      if (marquees.find(p) != marquees.end())
      {
        std::cout << std::setw(3) << std::right << marquees[p].profondeur << "|" << marquees[p].remontee << " ";
      }
      else
      {
        std::cout << std::setw(3) << std::right << -1 << "|" << -1 << " ";
      }
    }
    std::cout << std::endl;
  }
}

static const Position get_depart(Plateau &p)
{
  /*
  @brief Récupère la première case non-aménagée du plateau
  @param p Le plateau de jeu
  @return La position de la première case non-aménagée
  */
  for (auto &t : p.tuiles)
  {
    Position pos = t.first;
    Tuile &tuile = t.second;
    if (est_pas_amenagee(tuile))
    {
      return pos;
    }
  }
  return Position(-1, -1);
}

static void placer_routes(Plateau &p)
{
  /*
  @brief Place les routes sur le plateau de jeu pour que chaque case soit accessible
  @param p Le plateau de jeu
  */
  Position d = get_depart(p);
  if (d.first < 0 || d.second < 0)
  {
    return;
  }
  int nbr_util_d = 0;
  std::map<Position, Tuile_marquee> marquees = parcours_profondeur(p.tuiles, d);
  // afficher_parcour(p,marquees);
  for (auto &t : p.tuiles)
  {
    Position pos = t.first;
    Tuile &tuile = t.second;
    if (!est_pas_amenagee(tuile))
    {                      // On vérifie les aménagements qui sont accessible que par une case vide
      Position concurrent; // Position d'une case vide concurrente à devenir route
      int nbr_vide_route = 0;
      for (int i = 0; i < 4; ++i)
      {
        Position v = voisine(pos, i);
        if (p.tuiles.find(v) != p.tuiles.end())
        { // Si la tuile existe
          if (est_pas_amenagee(p.tuiles[v]))
          {
            nbr_vide_route++;
            concurrent = v;
          }
        }
      }
      if (nbr_vide_route == 1)
      { // Le cas 1
        p.tuiles[concurrent].amenagement = Amenagement::ROUTE;
      }
    }
    else
    {
      for (int i = 0; i < 4; ++i)
      {
        Position v = voisine(pos, i);
        if (p.tuiles.find(v) != p.tuiles.end())
        { // Si la tuile voisine existe
          if (est_pas_amenagee(p.tuiles[v]))
          {
            if ((pos.first != d.first || pos.second != d.second))
            {
              if (marquees[v].profondeur == marquees[pos].profondeur + 1 && marquees[v].remontee >= marquees[pos].profondeur)
              { // Le cas 2
                p.tuiles[pos].amenagement = Amenagement::ROUTE;
              }
            }
            if (pos.first == d.first && pos.second == d.second)
            { // Le cas 3
              if (marquees[v].profondeur == marquees[pos].profondeur + 1)
              {
                nbr_util_d++;
                if (nbr_util_d > 1)
                {
                  p.tuiles[pos].amenagement = Amenagement::ROUTE;
                }
              }
            }
          }
        }
      }
    }
  }
}

static int parcours_largeur(std::map<Position, Tuile> &tuiles, const Position &pos_depart)
{
  /*
  @brief Effectue un parcours en largeur du plateau en passant que par les cases
  routes et vides et en marquant toutes les cases vues durant le parcours
  @param tuiles La map du plateau de jeu
  @param pos_depart La position de départ du parcours
  @return Le nombre de cases marquées
  */
  std::map<Position, Tuile> marquees;
  std::queue<Position> a_voir;
  a_voir.push(pos_depart);
  marquees[pos_depart] = tuiles[pos_depart];
  while (!a_voir.empty())
  {
    Position pos = a_voir.front();
    a_voir.pop();
    for (int i = 0; i < 4; ++i)
    {
      Position v = voisine(pos, i);
      if (tuiles.find(v) != tuiles.end() && marquees.find(v) == marquees.end())
      {
        marquees[v] = tuiles[v];
        if (est_pas_amenagee(tuiles[v]))
        {
          a_voir.push(v);
        }
      }
    }
  }
  return marquees.size();
}

static void placer_routes_naive(Plateau &p)
{
  for (auto &t : p.tuiles)
  {
    Position pos = t.first;
    Tuile &tuile = t.second;
    // si la tuile est vide
    if (tuile.amenagement == Amenagement::VIDE)
    {
      // On récupère une tuile vide ou une route adjacente à la tuile courante
      p.tuiles[pos].amenagement = Amenagement::ARBRE;
      bool vide_autour = false; // si il y a une tuile vide autour de la tuile courante
      Position case_a_tester;
      for (int i = 0; i < 4; i++)
      {
        Position v = voisine(pos, i);
        if (p.tuiles.find(v) != p.tuiles.end() && est_pas_amenagee(p.tuiles[v]))
        {
          case_a_tester = v;
          vide_autour = true;
          break;
        }
      }
      if (vide_autour)
      {
        int nb_marque = parcours_largeur(p.tuiles, case_a_tester);
        if (nb_marque < p.tuiles.size())
        { // si il y a des cases non marquées, c'est une route
          p.tuiles[pos].amenagement = Amenagement::ROUTE;
        }
        else
        {
          p.tuiles[pos].amenagement = Amenagement::VIDE;
        }
      }
      else
      { // si il n'y a pas de tuile vide autour de la tuile courante c'est forcément une route
        p.tuiles[pos].amenagement = Amenagement::ROUTE;
      }
    }
  }
}

void Plateau::ajouter(const Position &pos)
{
  if (tuiles.find(pos) != tuiles.end())
  {
    throw std::domain_error(
        "une tuile existe déjà à cet endroit");
  }

  Tuile &nouveau = tuiles[pos];
  nouveau.amenagement = Amenagement::VIDE;
  nouveau.joueur = -1;
}

void Plateau::ajouter(const Position &pos_min, const Position &pos_max)
{
  for (int i = pos_min.first; i <= pos_max.first; ++i)
  {
    for (int j = pos_min.second; j <= pos_max.second; ++j)
    {
      try
      {
        ajouter({i, j});
      }
      catch (std::exception &e)
      {
        // erreur ignorée pour pouvoir simplement créer un plateau par blocs
      }
    }
  }
}

void Plateau::reserver(const Position &pos, int joueur)
{
  Tuile &t = tuiles.at(pos);

  if (t.joueur >= 0)
  {
    throw std::invalid_argument(
        "cette tuile est déjà réservée");
  }

  t.joueur = joueur;
  t.amenagement = Amenagement::RESERVEE;
  // placer_routes(*this);
}

void Plateau::amenager(const Position &pos, Amenagement amenagement, int joueur)
{
  Tuile &t = tuiles.at(pos);

  /*
    if(t.amenagement != Amenagement::RESERVEE) {
      throw std::invalid_argument(
          "cette tuile n'a pas été réservée"
          ) ;
    }

    if(t.joueur != joueur) {
      throw std::invalid_argument(
          "cette tuile n'est pas réservée pour le joueur"
          ) ;
    }
  */

  t.joueur = joueur;
  t.amenagement = amenagement;
  placer_routes(*this);
}

std::ostream &operator<<(std::ostream &out, const Plateau &plateau)
{
  // pas de tuile pas d'affichage
  if (plateau.tuiles.size() == 0)
    return out;

  // dimensions du plateau
  int lmin, lmax, cmin, cmax;
  lmin = lmax = plateau.tuiles.begin()->first.first;
  cmin = cmax = plateau.tuiles.begin()->first.second;
  for (auto &t : plateau.tuiles)
  {
    lmin = std::min(lmin, t.first.first);
    lmax = std::max(lmax, t.first.first);
    cmin = std::min(cmin, t.first.second);
    cmax = std::max(cmax, t.first.second);
  }

  // creation d'un buffer de suffisamment de lignes
  int eliminees = plateau.eliminees.size() > 0 ? 4 : 0;
  ConsolePad pad((lmax - lmin + 1) * 2 + 1 + eliminees);

  // dessin d'une tuile
  auto dessiner = [&](const Position &pos, const Tuile &tuile)
  {
    // ligne et colonne dans le pad
    unsigned int l = 2 * (pos.first - lmin);
    unsigned int c = 4 * (pos.second - cmin);

    // tuile vide
    pad.moveto(l, c);
    pad
        << "+   +" << std::endl
        << "     " << std::endl
        << "+   +";

    // murs
    static constexpr const int positions_murs[] = {
        0, 1,
        1, 4,
        2, 1,
        1, 0};
    for (int i = 0; i < 4; ++i)
    {
      Position v = voisine(pos, i);
      if (plateau.tuiles.find(v) == plateau.tuiles.end())
      {
        pad.moveto(l + positions_murs[2 * i], c + positions_murs[2 * i + 1]);
        if (i % 2 == 1)
        {
          pad << "|";
        }
        else
        {
          pad << "---";
        }
      }
    }

    // contenu possible des tuiles
    static constexpr const char *txt_amenagements[] = {
        "+ +", "+A+", "+B+", "+C+", "+D+", "+E+",
        "$ $", "$A$", "$B$", "$C$", "$D$", "$E$",
        "{:}",
        " # ",
        " @ ",
        "   "};
    static constexpr const char *couleurs_joueurs[] = {
#ifndef NO_COLOR
        "\x1B[38;5;38m",  // bleu
        "\x1B[38;5;208m", // orange
        "\x1B[38;5;135m", // violet
        "\x1B[38;5;11m",  // jaune
        "\x1B[38;5;206m"  // rose
                          //"\x1B[38;5;124m" //rouge
#else
        ""
#endif
    };
    static constexpr const int nb_couleurs = sizeof(couleurs_joueurs) / sizeof(char *);

    static constexpr const char *couleur_defaut =
#ifndef NO_COLOR
        "\x1B[39m"
#else
        ""
#endif
        ;

    pad.moveto(l + 1, c + 1);
    Amenagement base = type(tuile.amenagement);
    if (base == Amenagement::BOUTIQUE || base == Amenagement::RESERVEE)
    {
      pad << couleurs_joueurs[tuile.joueur % nb_couleurs];
    }
    pad << txt_amenagements[(int)tuile.amenagement];
    if (base == Amenagement::BOUTIQUE || base == Amenagement::RESERVEE)
    {
      pad << couleur_defaut;
    }
  };

  // dessin
  for (auto &t : plateau.tuiles)
  {
    dessiner(t.first, t.second);
  }

  // tuiles eliminees
  int nb_eliminees = plateau.eliminees.size();
  for (int i = 0; i < nb_eliminees; ++i)
  {
    dessiner({lmax + 2, cmin + i}, plateau.eliminees[i]);
  }

  out << pad.lines();
  return out;
}

#ifndef NO_GRAPHICS
#include <librsvg-2.0/librsvg/rsvg.h>
#include <cairo/cairo.h>
#include <cairo/cairo-svg.h>

void Plateau::dessiner(const std::string &cible, int unit, float margin, const std::string &style)
{
  // pas de tuile pas d'affichage
  if (tuiles.size() == 0)
    return;

  // dimensions du plateau
  int lmin, lmax, cmin, cmax;
  lmin = lmax = tuiles.begin()->first.first;
  cmin = cmax = tuiles.begin()->first.second;
  for (auto &t : tuiles)
  {
    lmin = std::min(lmin, t.first.first);
    lmax = std::max(lmax, t.first.first);
    cmin = std::min(cmin, t.first.second);
    cmax = std::max(cmax, t.first.second);
  }

  // chargement du fichier de style svg
#ifndef FF_ASSETS
#define FF_ASSETS "../assets/"
#endif
  std::stringstream style_path;
  style_path << FF_ASSETS << style;
  GError *svg_error = nullptr;
  RsvgHandle *style_handle = rsvg_handle_new_from_file(style_path.str().c_str(), &svg_error);
  if (svg_error)
  {
    throw std::invalid_argument("fichier de style introuvable");
  }

  // creation d'une surface cairo pour enregistrer le dessin
  cairo_surface_t *record = cairo_recording_surface_create(CAIRO_CONTENT_COLOR_ALPHA, nullptr);

  // contexte de dessin
  cairo_t *cr = cairo_create(record);

  // dessin d'site
  auto dessiner = [&](const Position &pos, const Tuile &tuile)
  {
    // fenetre a dessiner
    double x = pos.second;
    double y = pos.first;
    RsvgRectangle rect({x, y, 1, 1});

    // id dans le fichier de style svg
    static constexpr const char *ids[]{
        "producteur",
        "producteur_avocat",
        "producteur_brocoli",
        "producteur_carotte",
        "producteur_datte",
        "producteur_echalote",
        "boutique",
        "boutique_avocat",
        "boutique_brocoli",
        "boutique_carotte",
        "boutique_datte",
        "boutique_echalote",
        "inutile",
        "route",
        "reservee",
        "vide"};

    std::stringstream svg_id;
    svg_id << "#" << ids[(int)tuile.amenagement];
    Amenagement base = type(tuile.amenagement);
    if (base == Amenagement::BOUTIQUE || base == Amenagement::RESERVEE)
    {
      svg_id << "_" << tuile.joueur;
    }

    // dessin
    GError *error = nullptr;
    rsvg_handle_render_layer(style_handle, cr, svg_id.str().c_str(), &rect, &error);
    if (error)
    {
      std::cout << "got an error : " << svg_id.str() << " " << x << " " << y << std::endl;
    }
  };

  // dessin
  for (auto &t : tuiles)
  {
    dessiner(t.first, t.second);
  }

  // tuiles eliminees
  int nb_eliminees = eliminees.size();
  for (int i = 0; i < nb_eliminees; ++i)
  {
    dessiner({lmax + 2, cmin + i}, eliminees[i]);
  }

  cairo_surface_flush(record);

  // finalisation du dessin
  cairo_destroy(cr);
  g_object_unref(style_handle);

  // export
  double x0, y0, largeur, hauteur;
  cairo_recording_surface_ink_extents(record, &x0, &y0, &largeur, &hauteur);

  // il semble qu'il y ait un bug dans le calcul des largeur, hauteur, et origine
  largeur -= 2;
  hauteur -= 2;
  x0 += 1;
  y0 += 1;

  // ouvrir une surface pour l'export
  int umargin = margin * unit;
  cairo_surface_t *img;
  const std::string &extension = cible.substr(cible.find_last_of(".") + 1);
  if (extension == "svg")
  {
    img = cairo_svg_surface_create(
        cible.c_str(), unit * largeur + 2 * umargin, unit * hauteur + 2 * umargin);
  }
  else if (extension == "png")
  {
    img = cairo_image_surface_create(
        CAIRO_FORMAT_ARGB32, unit * largeur + 2 * umargin, unit * hauteur + 2 * umargin);
  }
  else
  {
    throw std::invalid_argument("seul l'export svg ou png est possible");
  }

  // creer un contexte pour l'export
  cr = cairo_create(img);
  cairo_translate(cr, umargin, umargin);
  cairo_scale(cr, unit, unit);

  // rejouer l'enregistrement
  cairo_set_source_surface(cr, record, -x0, -y0);
  cairo_paint(cr);
  cairo_show_page(cr);

  // finalisation de l'image
  cairo_surface_flush(img);

  // ecriture du png si besoin
  if (extension == "png")
  {
    cairo_surface_write_to_png(img, cible.c_str());
  }

  // destruction du contexte
  cairo_destroy(cr);

  // destruction de la surface d'export
  cairo_surface_finish(img);
  cairo_surface_destroy(img);
  cairo_surface_finish(record);
  cairo_surface_destroy(record);
}

#endif
